extends KinematicBody2D;

const speed=20000;
var vel = Vector2();
var animator;
var play;
var bullet = preload("res://objects/items/P_Projectile.tscn");
var weapid = 0;
var paused = false
var log_timer_start = false
var log_timer = 0
var log_show_time = 5

func _ready():
	g.pp = position
	g.gpp = global_position
	animator = get_node("animSprite");
	animator.set_animation("idle");
	play = true
	
func _process(delta):
	
	
	$"camera/dead_menu".visible = false
	g.pp = position
	g.gpp = global_position
	$"left_hand".get_child(g.weapid).visible = false
	$"left_hand".get_child(g.weapid).visible = true
	
	
	move_function(delta);
	
#	print (g.p_mana_full)
	
	$"camera/ui/mana_bar".max_value = g.p_mana_full
	$"camera/ui/mana_bar".value = g.p_mana;
	$"camera/ui/health_bar".max_value = g.p_max_hp
	$"camera/ui/health_bar".value = g.p_hp;
	
	if log_timer_start == true:
		log_timer += delta
		$"camera/ui/log".text = ""
		if log_timer > log_show_time:
			log_timer = 0
			log_timer_start = false
	
	if g.p_mana < g.p_mana_full:
		g.p_mana += g.mana_restore
	
	if Input.is_key_pressed(KEY_ESCAPE) && paused == false:
		get_tree().change_scene("res://objects/menu.tscn")
	
	if Input.is_action_just_pressed('fire') && !g.talking:
		fire()
	
	if g.p_hp <= 0:
#		get_tree().paused = true;
		$"camera/dead_menu".visible = true
		$"camera/dead_menu/restart".visible = true
#		g.level1_state["enemy1_islive"] = true
#		g.level1_state["enemy2_islive"] = true
#		g.level1_state["enemy3_islive"] = true
		# TODO: Fix this
		g.restore_all_enemies()
		g.inventory[0].clear()
		g.inventory[1].clear()
		g.weapid = 0
		g.talking = false
	
	if g.xp >= g.xp_max:
		levelUp()
		
	if Input.is_action_just_pressed("character_menu"):
		showLevelUpMenu()
		$"camera/character_menu".call("update")
		g.talking = !g.talking
		if g.needToCalculate:
			g.charsCalculate()
			g.needToCalculate = false
	
	$left_hand.look_at(get_global_mouse_position())
	
	if Input.is_action_just_pressed("inventory_call"):
		$"camera/inv".visible = !$"camera/inv".visible
		g.talking = !g.talking
		showInventory()
	
	weapon_change()


func weapon_change():
	if Input.is_action_just_pressed("weapon_next"):
		if $left_hand.get_child(g.weapid+1) != null:
			$"left_hand".get_child(g.weapid).visible = false
			g.weapid += 1
			$"left_hand".get_child(g.weapid).visible = true
	elif Input.is_action_just_pressed("weapon_previous"):
		if $left_hand.get_child(g.weapid-1) != null:
			$"left_hand".get_child(g.weapid).visible = false
			g.weapid -= 1
			$"left_hand".get_child(g.weapid).visible = true
		
func weapon_change___():
	if Input.is_key_pressed(KEY_1):
		$"left_hand".get_child(g.weapid).visible = false
		g.weapid = 0
		$"left_hand".get_child(g.weapid).visible = true
		print(1)
	elif Input.is_key_pressed(KEY_2) and $"left_hand".has_node("dagger"):
		$"left_hand".get_child(g.weapid).visible = false
		g.weapid = 1
		$"left_hand".get_child(g.weapid).visible = true
		print(2)

func move_function(delta):
	if(Input.is_action_pressed('ui_left') and Input.is_action_pressed('ui_up')):
		animator.play("run_up");
		play = false;
	if(Input.is_action_pressed('ui_right') and Input.is_action_pressed('ui_up')):
		animator.play("run_up");
		play = false;
	if(Input.is_action_pressed('ui_right') and Input.is_action_pressed('ui_down')):
		animator.play('run_down');
		play = false
	if(Input.is_action_pressed('ui_left') and Input.is_action_pressed('ui_down')):
		animator.play('run_down');
		play = false
	
	if (Input.is_action_pressed('ui_left')):
		if(play == true):
			animator.play("run_left");
		vel.x -= 1;
	elif (Input.is_action_pressed('ui_right')):
		vel.x += 1;
		if(play == true):
			animator.play('run_right');
	else:
		vel.x=0;
	
	if (Input.is_action_pressed('ui_up')):
		if(play == true):
			animator.play("run_up");
		vel.y -= 1;
	elif (Input.is_action_pressed('ui_down')):
		vel.y += 1;
		if(play == true):
			animator.play('run_down');
	else:
		vel.y=0;
		
	#releases
	if(Input.is_action_just_released('ui_down')):
		animator.play("idle");
		vel.y=0;
		play = true;
	if(Input.is_action_just_released('ui_up')):
		animator.play("idle");
		vel.y=0;
		play = true;
	if(Input.is_action_just_released('ui_left')):
		animator.play("idle");
		vel.x=0;
		play = true;
	if(Input.is_action_just_released('ui_right')):
		animator.play("idle");
		vel.x=0;
		play = true;
	#releases
	
	vel = vel.normalized();
	move_and_slide(vel*speed*delta);

func fire():
	$"left_hand".get_child(g.weapid).call("fire");

func tolog (message:String):
	log_timer_start = true
	$"camera/ui/log".text = message

func levelUp():
	g.needToCalculate = true
	
	
	if g.xp > g.xp_max:
		g.xp = (g.xp - g.xp_max)
	g.level += 1
	if g.aviablePerks == g.maxAviablePerks:
		g.maxAviablePerks += 3
		g.aviablePerks = g.maxAviablePerks
	elif g.aviablePerks == 0 && g.maxAviablePerks != g.defAviablePerks:
		g.maxAviablePerks = g.defAviablePerks
		g.aviablePerks = g.maxAviablePerks
	elif g.aviablePerks != 0 && g.aviablePerks != g.maxAviablePerks:
		g.aviablePerks = g.maxAviablePerks
		g.maxAviablePerks += 3
		g.aviablePerks = g.maxAviablePerks
	else:
		g.aviablePerks = g.maxAviablePerks
	g.xp_max += 5
	
	showLevelUpMenu()
	$"camera/character_menu".call("update")
	print(g.aviablePerks)
	print(g.maxAviablePerks)
	var string = "Новый уровень! Ты достиг "+str(g.level)+" уровня!"
	tolog("Новый уровень! Ты достиг "+str(g.level)+" уровня!")
	print("Новый уровень! Ты достиг "+str(g.level)+" уровня!")
	g.talking = true

func showLevelUpMenu():
	$"camera/character_menu/levelUp".call("update")
	$"camera/character_menu".visible = !$"camera/character_menu".visible

func showInventory():
	$"camera/inv/Panel".clear()
	
	print("--")
	for item in invwor.inventory[0]:
		print(item[1])
		var i = load(item[3]).instance()
		var icon
		for sprite in i.get_children():
			if sprite.name == "sprite":
				icon = sprite
		if !icon.region_enabled:
			$"camera/inv/Panel".add_item(item[1], icon)
		else:
			$"camera/inv/Panel".add_item(item[1], icon.texture)
			$"camera/inv/Panel".set_item_icon_region($"camera/inv/Panel".get_item_count()-1, icon.region_rect)

func _on_Area2D_body_entered(body):
	if body.has_method("damage"):
		var dmg = body.call("damage")
		print ("-"+str(dmg)+" hp to player");
		g.p_hp -= (dmg*g.difficult)
