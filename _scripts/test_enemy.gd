extends KinematicBody2D


export var health = 40;
var timer = 0;

export var trigger_dist = 400;
export var run_dist = 170;

var isRun = false;
export var timeToRun = 1.7;

var pdir;
export var speed = 200;
export var run_speed = 120;
var xp = 5

func _ready():
	print(name + "_islive")
	print(g.Start_Location_state)
	if g.Start_Location_state[name+"_islive"] == false:
		queue_free()

func _process(delta):

	if health <= 0:
		g.xp += xp
		g.level1_state[name+"_islive"] = false
		queue_free();

	var hp = $"health_bar";
	hp.value = health

	if position.distance_to(g.pp) < trigger_dist and !isRun and g.debug == false:
		pdir = g.pp - position;
		move_and_collide(pdir.normalized()*speed*delta)


func hurt(damage):
	health -= damage*(g.difficult/2)
