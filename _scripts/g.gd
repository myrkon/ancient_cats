extends Node

var pp;
var gpp;

var p_mana = 100;
var p_mana_full = 100;
var mana_restore = 0.5;

var level = 1
var xp = 0
var xp_max = 5
var aviablePerks = 0
var defAviablePerks = 3
var maxAviablePerks = defAviablePerks
var chars = {
	"int":5, 
	"magpow": 5,
	"str": 5
}

var difficult = 2
var damage = 0
var mag_damage = 0

var p_hp = 150;
var p_max_hp = 150;

var weapid = 0;

var level1_state = {
	"enemy1_islive": true,
	"enemy2_islive": true,
	"enemy3_islive": true,
	"crazy_NPC_islive": true
}

var Start_Location_state = {
	"saby_islive": true,
	"test_enemy_islive": true,
	"test_enemy2_islive": true,
	"test_enemy3_islive": true
}

var spawnRequest = {
	"level":"",
	"point":""
}

func restore_all_enemies():
	for enemy in level1_state:
		enemy = true
	
	for enemy in Start_Location_state:
		enemy = true

var talking = false

var needToCalculate = false

var debug = false

func charsCalculate():
	
	p_max_hp += chars["str"]
	damage += floor(chars["str"] / 2)
	mag_damage += floor(chars["magpow"] / 2)
	
	print("mana_max before:"+str(p_max_hp))
	p_mana_full += chars["int"]*2
	p_mana = p_mana_full
	print("mana_max after:"+str(p_max_hp), ", прирост:"+str(chars["int"]*2))
	
