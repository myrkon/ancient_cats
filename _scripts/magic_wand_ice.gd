extends StaticBody2D

var bullet = preload("res://objects/items/P_Projectile.tscn");


func fire ():
	if g.p_mana > 15:
		var bul = bullet.instance();
		bul.position = $start_bul_pos2.global_position
		var imp = (get_global_mouse_position() - g.gpp).normalized()
		get_node("../../../../").add_child(bul);
		bul.apply_impulse(Vector2(), imp * 800);
		g.p_mana -= 15;
