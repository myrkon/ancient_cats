extends KinematicBody2D


export var health = 100;
var timer = 0;

export var trigger_dist = 10000;
export var run_dist = 170;

var isRun = false;
export var timeToRun = 1.7;

var xp = 2

var pdir;
export var speed = 200;
export var run_speed = 120;

func _process(delta):
	
	if health <= 0:
		g.xp += xp
		g.level1_state[name+"_islive"] = false
		queue_free();

	var hp = $"health_bar";
	hp.value = health
	
	if position.distance_to(g.pp) < trigger_dist and !isRun:
		pdir = g.pp - position;
		move_and_collide(pdir.normalized()*speed*delta)
#	if position.distance_to(g.pp) < run_dist:
#		isRun = true
#		pdir = g.pp + position;
#		move_and_collide(pdir.normalized()*speed*delta)
#
#	if isRun:
#		timer += delta
#		pdir = g.pp + position;
#		move_and_collide(pdir.normalized()*run_speed*delta)
#		if timer >= timeToRun:
#			timer = 0;
#			isRun = false;

	
func hurt(damage):
	health -= damage*(g.difficult/2)
