extends Control

func update():
	$"aviablePerks".text = "Доступно перков:" + str(g.aviablePerks)+"/"+str(g.maxAviablePerks)
	$"intellegence".text = "Интеллект:" + str(g.chars["int"])
	$"strength".text = "Сила:" + str(g.chars["str"])
	$"magick_power".text = "Магия:" + str(g.chars["magpow"])
	$"LevelUp".text = "Уровень "+str(g.level)
	
func hide():
	$"..".visible = false
	g.needToCalculate = false
