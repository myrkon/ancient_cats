extends TextureButton


func _on_add_pressed():
	if g.aviablePerks > 0 and g.aviablePerks <= g.maxAviablePerks && g.needToCalculate:
		$"..".text = "Сила:"+str(g.chars["str"]+1)
		g.chars["str"] += 1
		g.aviablePerks -= 1
		$"../../aviablePerks".text = "Доступно перков:" + str(g.aviablePerks)+"/"+str(g.maxAviablePerks)
