extends TextureButton

func _on_del_pressed():
	if g.aviablePerks > -1 and g.aviablePerks < g.maxAviablePerks and g.needToCalculate:
		g.chars["int"] -=  1
		$"..".text = "Интеллект:"+str(g.chars["int"])
		g.aviablePerks += 1
		$"../../aviablePerks".text = "Доступно перков:" + str(g.aviablePerks)+"/"+str(g.maxAviablePerks)
