<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="Interior" tilewidth="16" tileheight="16" tilecount="256" columns="16">
 <image source="tileset_16x16_interior.png" width="256" height="256"/>
 <terraintypes>
  <terrain name="Roof" tile="32"/>
  <terrain name="Carpet red" tile="80"/>
  <terrain name="Carpet green" tile="128"/>
  <terrain name="Carpet blue" tile="176"/>
 </terraintypes>
 <tile id="32" terrain=",,,0"/>
 <tile id="33" terrain=",,0,0"/>
 <tile id="34" terrain=",,0,"/>
 <tile id="35" terrain="0,0,0,"/>
 <tile id="36" terrain="0,0,,0"/>
 <tile id="48" terrain=",0,,0"/>
 <tile id="49" terrain="0,0,0,0"/>
 <tile id="50" terrain="0,,0,"/>
 <tile id="51" terrain="0,,0,0"/>
 <tile id="52" terrain=",0,0,0"/>
 <tile id="64" terrain=",0,,"/>
 <tile id="65" terrain="0,0,,"/>
 <tile id="66" terrain="0,,,"/>
 <tile id="80" terrain=",,,1"/>
 <tile id="81" terrain=",,1,1"/>
 <tile id="82" terrain=",,1,"/>
 <tile id="83" terrain="1,1,1,"/>
 <tile id="84" terrain="1,1,,1"/>
 <tile id="96" terrain=",1,,1"/>
 <tile id="97" terrain="1,1,1,1"/>
 <tile id="98" terrain="1,,1,"/>
 <tile id="99" terrain="1,,1,1"/>
 <tile id="100" terrain=",1,1,1"/>
 <tile id="112" terrain=",1,,"/>
 <tile id="113" terrain="1,1,,"/>
 <tile id="114" terrain="1,,,"/>
 <tile id="128" terrain=",,,2"/>
 <tile id="129" terrain=",,2,2"/>
 <tile id="130" terrain=",,2,"/>
 <tile id="131" terrain="2,2,2,"/>
 <tile id="132" terrain="2,2,,2"/>
 <tile id="144" terrain=",2,,2"/>
 <tile id="145" terrain="2,2,2,2"/>
 <tile id="146" terrain="2,,2,"/>
 <tile id="147" terrain="2,,2,2"/>
 <tile id="148" terrain=",2,2,2"/>
 <tile id="160" terrain=",2,,"/>
 <tile id="161" terrain="2,2,,"/>
 <tile id="162" terrain="2,,,"/>
 <tile id="176" terrain=",,,3"/>
 <tile id="177" terrain=",,3,3"/>
 <tile id="178" terrain=",,3,"/>
 <tile id="179" terrain="3,3,3,"/>
 <tile id="180" terrain="3,3,,3"/>
 <tile id="192" terrain=",3,,3"/>
 <tile id="193" terrain="3,3,3,3"/>
 <tile id="194" terrain="3,,3,"/>
 <tile id="195" terrain="3,,3,3"/>
 <tile id="196" terrain=",3,3,3"/>
 <tile id="208" terrain=",3,,"/>
 <tile id="209" terrain="3,3,,"/>
 <tile id="210" terrain="3,,,"/>
</tileset>
