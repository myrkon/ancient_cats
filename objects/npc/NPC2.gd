extends KinematicBody2D

var replicas = {
	"replica0":["Блин каракальская дева такая крутая!", "Согласен, кхм, хорошая книга", "а что это?", "Нет, так себе", ""],
	"replica1":["Лучше сам прочитай, поймешь меня, Она на столе", "[Продолжить]", "", "", ""],
	"replica2":["Ну хз, мне нравится", "[Продолжить]", "", "", ""],
	"replica3":["Здравствуй.","А что за придурок за бочкой на улице?","","",""],
	"replica4":["Согласен, солидный с виду тигр. Вчера прибыл откуда-то.","Спасибо","","",""],
}

var currentReplica
var currentReplicaIndex = 0


func _on_Area2D_body_entered(body):
	if g.talking == false:
		if body.name == "Player":
			if journal.level1["crazy"] == "true": showDialog(3)
			else: showDialog(0)

func showDialog(index):
	g.talking = true
	
	$"dialog".visible = true
	currentReplica = replicas["replica"+str(index)]
	
	$dialog/ItemList.clear()
	
	for i in range(1, 5):
		$"dialog/ItemList".add_item(currentReplica[i])
	$"dialog/replicaText".text = currentReplica[0]
	currentReplicaIndex += 1

func _on_ItemList_item_activated(index):
	print (currentReplica[index+1], ":" ,currentReplicaIndex)
	
	if currentReplicaIndex == 1 and journal.level1["tigr"] != "true":
		if currentReplica[index+1] == replicas["replica0"][2]:
			showDialog(1)
		elif currentReplica[index+1] == replicas["replica0"][3]:
			showDialog(2)
		else:
			$"dialog".visible = false
			g.talking = false
			currentReplicaIndex = 0
	elif currentReplicaIndex == 1 and journal.level1["tigr"] == "true":
		showDialog(4)
		journal.level1["tigr"] = ""
	else:
		hide()

func _on_Area2D_body_exited(body):
	if body.name == "Player":
		$"dialog".visible = false
		g.talking = false

func hide():
	$"dialog".visible = false
	g.talking = false
	currentReplicaIndex = 0