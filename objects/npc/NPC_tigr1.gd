extends KinematicBody2D

var replicas = {
	"replica0":["чо по алгебре задали?", "У нас было 2 пакетика травы, 75 ампул мескалина, 5 пакетиков диэтиламид лизергиновой кислоты или ЛСД", "плохо", "отлично", "иди нахуй"],
	"replica1":["Я собак! Мууууу", "извинись", "", "", ""],
	"replica2":["Татарстан супер гуд!", "извини", "", "", ""]
}

var currentReplica
var currentReplicaIndex = 0


func _on_Area2D_body_entered(body):
	if g.talking == false:
		if body.name == "Player":
			showDialog(0)
			journal.level1["tigr"] = "true"

func showDialog(index):
	g.talking = true
	
	$"dialog".visible = true
	currentReplica = replicas["replica"+str(index)]
	
	$dialog/ItemList.clear()
	
	for i in range(1, 5):
		$"dialog/ItemList".add_item(currentReplica[i])
	$"dialog/replicaText".text = currentReplica[0]
	currentReplicaIndex += 1

func _on_ItemList_item_activated(index):
	print (currentReplica[index+1], ":" ,currentReplicaIndex)
	
	if currentReplicaIndex == 1:
		if currentReplica[index+1] == replicas["replica0"][1]:
			showDialog(1)
		else:
			$"dialog".visible = false
			g.talking = false
			currentReplicaIndex = 0
	elif currentReplicaIndex == 2:
		if currentReplica[index+1] == replicas["replica1"][1]:
			showDialog(2)
	elif currentReplicaIndex == 3:
		hide()

func _on_Area2D_body_exited(body):
	if body.name == "Player":
		$"dialog".visible = false
		g.talking = false

func hide():
	$"dialog".visible = false
	g.talking = false
	currentReplicaIndex = 0
