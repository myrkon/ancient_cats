extends KinematicBody2D

var id = "crazy"
var level = "level1"
var jsonData
var curr_dialog_id = 0
var trigger_id = "none"

var angry = false
var hp = 50
var maxhp = 50
var timer = 0;

export var trigger_dist = 400;
export var run_dist = 170;

var isRun = false;
export var timeToRun = 1.7;

var pdir;
export var speed = 200;
export var run_speed = 120;
var xp = 5

func _ready():
	var jsonFile = File.new()
	jsonFile.open("res://npcReplicas.json", File.READ)
	var content = jsonFile.get_as_text()
		
	jsonFile.close()
	jsonData = JSON.parse(content)
	
	#trigger content
	journal.level1["crazy"] = "true"

func _physics_process(delta):
	if hp <= 0:
		g.xp += xp
		g.level1_state[name+"_islive"] = false
		queue_free();
		
	$hp_bar.value = hp
	
	$"hp_bar".value = hp
	
	if curr_dialog_id == 2:
		isRun = true
		close_dialog_form()
		$"hp_bar".visible = true
	
	if position.distance_to(g.pp) < trigger_dist and isRun:
		pdir = g.gpp - position;
		move_and_collide(pdir.normalized()*speed*delta)


func get_text_from_json(msg_id):
	return jsonData.result[level][id][trigger_id][str(msg_id)][0]

func get_ans(msg_id, ans_id):
	return jsonData.result[level][id][trigger_id][str(msg_id)][ans_id]

func show_dialog_form():
	$"menu_pyam".visible = true

func close_dialog_form():
	$"menu_pyam".visible = false

func draw_dialog(msg_id):
	$"menu_pyam/Speak".text = get_text_from_json(msg_id)
	$"menu_pyam/ItemList".clear()
	for i in range(1, 4):
		print("answear "+str(i)+" "+get_ans(msg_id, i)[0])
		$"menu_pyam/ItemList".add_item(get_ans(msg_id, i)[0])

func _on_ItemList_item_activated(index):
	var id = index+1
	var ans = get_ans(curr_dialog_id, id)
	if ans[1] == "exit":
		close_dialog_form()
	elif ans[1] != "":
		var ans_id = int(ans[1])
		curr_dialog_id = ans_id
		draw_dialog(curr_dialog_id)


func _on_Area2D_body_entered(body):
	if body.name == "Player":
		g.talking = true
		show_dialog_form()
		draw_dialog(0)
		print("Player triggered "+name)


func _on_Area2D_body_exited(body):
	if body.name == "Player":
		close_dialog_form()
		g.talking = false
		curr_dialog_id = 0
		trigger_id = "none"

func hurt(damage):
	hp -= damage