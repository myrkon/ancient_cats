extends Area2D

var id = "test_npc"
var jsonData
var curr_dialog_id = 0
var trigger_id = "none"

func _ready():
	var jsonFile = File.new()
	jsonFile.open("res://npcReplicas.json", File.READ)
	var content = jsonFile.get_as_text()
		
	jsonFile.close()
	jsonData = JSON.parse(content)

func get_text_from_json(msg_id):
	return jsonData.result["level1"][id][trigger_id][str(msg_id)][0]

func get_ans(msg_id, ans_id):
	return jsonData.result["level1"][id][trigger_id][str(msg_id)][ans_id]

func show_dialog_form():
	$"menu_pyam".visible = true

func close_dialog_form():
	$"menu_pyam".visible = false

func _on_json_NPC_body_entered(body):
	if body.name == "Player":
		g.talking = true
		show_dialog_form()
		draw_dialog(0)
		print("Player triggered "+name)

func draw_dialog(msg_id):
	$"menu_pyam/Speak".text = get_text_from_json(msg_id)
	$"menu_pyam/ItemList".clear()
	for i in range(1, 4):
		print("answear "+str(i)+" "+get_ans(msg_id, i)[0])
		$"menu_pyam/ItemList".add_item(get_ans(msg_id, i)[0])

func _on_ItemList_item_activated(index):
	var id = index+1
	var ans = get_ans(curr_dialog_id, id)
	if ans[1] == "exit":
		close_dialog_form()
	elif ans[1] != "":
		var ans_id = int(ans[1])
		curr_dialog_id = ans_id
		draw_dialog(curr_dialog_id)

func _on_json_NPC_body_exited(body):
	if body.name == "Player":
		close_dialog_form()
		g.talking = false
		curr_dialog_id = 0
		trigger_id = "none"
