extends Node2D

var res = "res://objects/items/meat.tscn"

func _ready():
	for item in g.inventory[0]:
		if item == res:
			queue_free()

func _on_meat_body_entered(body):
	if body.name == "Player":
		body.call("add_item", load(res))
		g.inventory[0].append(res)
		queue_free()
