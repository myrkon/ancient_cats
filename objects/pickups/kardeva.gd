extends Area2D

var res = "res://objects/book/kardev1.tscn"

func _ready():
	for item in g.inventory[0]:
		if item == res:
			queue_free()

func _on_kardeva_pickup_body_entered(body):
	if body.name == "Player":
		body.call("add_item", load(res))
		g.inventory[0].append(res)
		queue_free()

