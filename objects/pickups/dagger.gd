extends Area2D

var res = "res://objects/items/dagger.tscn"

func _ready():
	for item in g.inventory[1]:
		if item == res:
			queue_free()

func _on_dagger_pickup_body_entered(body):
	if body.name == "Player":
		body.call("add_item", load(res))
		g.inventory[1].append(res)
		queue_free()
